import './App.css';
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { getAllProducts } from './redux/actions/products.action.js';

function App() {
  const dispatch = useDispatch();

  const productsGetAll = useSelector((state) => state.productsGetAll);
  const { loading, error, products, success } = productsGetAll;

  useEffect(() => {
    if (!products) {
      dispatch(getAllProducts());
    }
  }, [dispatch]);

  return (
    <div className='App'>
      {loading && <p>loading</p>}
      {error && <p>{error}</p>}
      {products &&
        products.map((product) => {
          return <p>{product.title}</p>;
        })}
    </div>
  );
}

export default App;
