import {
  PRODUCT_GET_ALL_FAIL,
  PRODUCT_GET_ALL_REQUEST,
  PRODUCT_GET_ALL_SUCCESS,
} from '../constants/products.contants';

export const productsGetAllReducer = (state = {}, action) => {
  switch (action.type) {
    case PRODUCT_GET_ALL_REQUEST:
      return { loading: true, products: [] };

    case PRODUCT_GET_ALL_SUCCESS:
      return {
        loading: false,
        products: action.payload,
        success: true,
      };

    case PRODUCT_GET_ALL_FAIL:
      return { loading: false, error: 'ERROR FINDING PRODUCTS' };

    default:
      return state;
  }
};
