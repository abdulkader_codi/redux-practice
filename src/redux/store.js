import { createStore, combineReducers, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';
import { productsGetAllReducer } from './reducers/products.reducer';

const reducer = combineReducers({
  productsGetAll: productsGetAllReducer,
});

const initialState = {
  theme: {
    loading: true,
  },
};

const middleware = [thunk];

const store = createStore(
  reducer,
  initialState,
  composeWithDevTools(applyMiddleware(...middleware))
);

export default store;
