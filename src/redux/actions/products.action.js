import axios from 'axios';
import {
  PRODUCT_GET_ALL_FAIL,
  PRODUCT_GET_ALL_REQUEST,
  PRODUCT_GET_ALL_SUCCESS,
} from '../constants/products.contants';

export const getAllProducts = () => async (dispatch) => {
  try {
    dispatch({
      type: PRODUCT_GET_ALL_REQUEST,
    });

    const { data } = await axios.get(
      `https://jsonplaceholder.typicode.com/todos`
    );

    dispatch({
      type: PRODUCT_GET_ALL_SUCCESS,
      payload: data,
    });
  } catch (error) {
    dispatch({
      type: PRODUCT_GET_ALL_FAIL,
    });
  }
};
